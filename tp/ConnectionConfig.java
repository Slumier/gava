package tp;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConnectionConfig {

    private String ip,tcpPort,rmiPort,nbPrinter;


    public ConnectionConfig() {
        try ( FileInputStream input = new FileInputStream( "config.properties" ) ){
            Properties props = new Properties();

            props.load(input);
            ip = props.getProperty("ip");
            tcpPort = props.getProperty("tpc.port");
            rmiPort = props.getProperty("rmi.port");
            nbPrinter = props.getProperty("number.printer");


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getIp() {
        return ip;
    }

    public int getTcpPort() {
        return Integer.parseInt(rmiPort);
    }

    public int getNbPrinter() {
        return Integer.parseInt(nbPrinter);
    }

    public int getRmiPort() {
        return Integer.parseInt(rmiPort);
    }

}
