package tp;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.AccessException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Properties;


public class Server {
    public static void main(String[] args) {
        InputStream inStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
        System.out.println(inStream);
        Properties props = new Properties();
        try {
            props.load(inStream);

            final int port = Integer.valueOf(props.getProperty("rmi.port"));
            Registry doc = LocateRegistry.createRegistry(port);


        } catch (NumberFormatException | IOException e) {
            System.out.println(e.getMessage());

        }

        class ServerImpression {
            public boolean imprime(String doc) {
                //accepte ou non l'impression, à voir avec TCP vers l'imprimante

                return false;
            }

            public int finishToPrint() {
                //mettre num imprimante via TCP
                return 1;
            }

        }
    }
}
