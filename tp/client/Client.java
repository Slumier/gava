package tp.client;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RMIClassLoader;
import java.util.Properties;

public class Client {
    public Client(String imp) throws Exception{
        try {
            InputStream inStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
            Properties props = new Properties();
            props.load(inStream);
            System.out.println("Client démarré");
            final int port = Integer.valueOf(props.getProperty("rmi.port"));
            Registry doc = LocateRegistry.getRegistry(port);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            //e.printStackTrace();
        }

    }
}
