package tp.client;

import javax.swing.*;
import javax.swing.plaf.synth.SynthTextAreaUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Properties;


public class GUIClient extends JFrame implements ActionListener {

    protected static final int WIDTH = 800;
    protected static final int HEIGHT = 500;
    protected Container container = getContentPane();
    protected JLabel TextPrint = new JLabel("Texte à imprimer");
    protected JTextField TextPrintField = new JTextField();
    protected JButton New = new JButton("New Print");
    protected DefaultListModel model;
    protected DefaultListModel model2;
    protected JList list;
    protected JList list2;
    public String message;









    // constructeur
    public GUIClient() {
        // creation de la fenetre
        setLayout(new BorderLayout());
        model = new DefaultListModel();
        list = new JList(model);
        model.addElement("Impression en attente");

        model2 = new DefaultListModel();
        list2 = new JList(model2);
        model2.addElement("Imprimé");

        this.add(list);
        this.add(list2);
        this.setTitle("Impression");
        this.setSize(WIDTH, HEIGHT);
        setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
        this.setLayoutManager();
        this.setLocationAndSize();
        this.addActionEvent();
        this.addComponentsToContainer();



    }

    public void setLayoutManager() {
        container.setLayout(null);
    }

    public void actionPerformed(ActionEvent e) {

        // Si la source est le bouton Send
        if (e.getSource() == New) {
            if(JOptionPane.showConfirmDialog(null,"Imprimer ?","Ye No Option",  JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){

                try {
                    String message = TextPrintField.getText();
                    Client(message);
                    model.addElement(message);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }

        // Si la source est le bouton Get

    }

    // parametrage de tous les elements de la fenetre
    public void setLocationAndSize()
    {
        TextPrint.setBounds(100, 10, 100, 30);
        TextPrintField.setBounds(100, 40, 100, 30);
        New.setBounds(400, 40, 100, 30);
        //testButton.setBounds(100, 330, 225, 30);
        list.setBounds(100, 70,200,400);
        list2.setBounds(300,70,200,400);
        //close.setBounds(100, 370, 225, 30);
    }

    // Ajout des composants dans le container
    public void addComponentsToContainer() {
        container.add(TextPrint);
        container.add(TextPrintField);
        container.add(New);
        container.add(list);
        container.add(list2);
    }

    public void addActionEvent() {
        New.addActionListener(this);
    }
    public void Client(String imp) throws Exception{
        try {
            InputStream inStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
            Properties props = new Properties();
            props.load(inStream);
            System.out.println("Client démarré");
            final int port = Integer.valueOf(props.getProperty("rmi.port"));
            Registry doc = LocateRegistry.getRegistry(port);
            Message mes =(Message) doc.lookup(imp);
            String returnal = mes.showMsg("Retour");
            System.out.println(returnal);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            //e.printStackTrace();
        }

    }



}
