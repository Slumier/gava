package tp.printer.pool;

import java.util.List;

public class HandlerPrinter {

    private final static PoolPrinter cc = new PoolPrinter();

    public static Printer takePrinter() throws InterruptedException {
        return cc.getPrinter();
    }

    public static void realesePrinter( Printer printer ) {
        cc.setPrinter(printer);
    }

    public static String getPool() {
        return cc.getPoolContent();
    }

}
