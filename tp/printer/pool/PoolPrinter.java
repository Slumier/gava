package tp.printer.pool;

import tp.ConnectionConfig;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PoolPrinter {

    private final ConnectionConfig cc = new ConnectionConfig();
    private final Set<Printer> printerPool = new HashSet<>();

    public PoolPrinter () {
        Printer printer;
        for ( int i = 1; i < cc.getNbPrinter(); i++ ) {
            printer = new Printer(i,"HP-500","HP");
            printerPool.add(printer);
        }
    }

    public synchronized Printer getPrinter() throws InterruptedException {

        Printer printer = null;

        while ( printerPool.isEmpty() ) {
            try {
                wait(1000);
                System.out.println("Waiting");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }


        if ( !printerPool.isEmpty() ) {
            wait(300);
            Printer[] tpmList = this.printerPool.toArray(new Printer[0]);
            printer = tpmList[0];
            this.printerPool.remove(printer);
            notifyAll();
            System.out.println("Printer had been taken");
        }

        return printer;
    }

    public void setPrinter(Printer printer) {
        if ( printer != null ) {
            printerPool.add(printer);
        } else {
            System.out.println("error : printer null");
        }
    }

    public String getPoolContent() {
        String msg = "";
        for ( int i = 1; i < this.printerPool.size(); i++ ) {
            Printer[] tpmList = this.printerPool.toArray(new Printer[i]);
            if ( i != this.printerPool.size() ) {
                msg += tpmList[i].toString() + "/";
            }

        }
        return msg;
    }

}
