package tp.printer.pool;

public class Printer {

    private int id;
    private String name, constructor;

    public Printer(int id, String name, String constructor) {
        this.id = id;
        this.name = name;
        this.constructor = constructor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConstructor() {
        return constructor;
    }

    public void setConstructor(String constructor) {
        this.constructor = constructor;
    }

    @Override
    public String toString() {
        return "Printer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", constructor='" + constructor + '\'' +
                '}';
    }
}
