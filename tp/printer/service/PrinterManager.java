package tp.printer.service;

import tp.printer.pool.*;

import java.io.*;
import java.net.Socket;

public class PrinterManager extends Thread {

    private final Socket socket;
    private final Printer printer;

    public PrinterManager( Socket socket, Printer printer ) {
        this.socket = socket;
        this.printer = printer;
    }

    public void run(){
        boolean bo = true;

        while ( bo ) {

            try {
                InputStream input = socket.getInputStream();
                DataInputStream dataInputStream = new DataInputStream(input);
                String msg = dataInputStream.readUTF();
                System.out.println("received :" + msg);


                if ( msg.split("/")[0] == "accept" ) {
                    System.out.println("yes");
                    msg = msg.split("/")[0];
                }
                switch (msg) {
                    case "getPool" -> msg = HandlerPrinter.getPool().toString();
                    case  "accept" -> msg = "printed";
                    case default -> msg = "yes";
                }

                System.out.println("send :" + msg);
                OutputStream outputStream = socket.getOutputStream();
                DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

                dataOutputStream.writeUTF( msg );
                dataOutputStream.flush();

            } catch (IOException e) {
                e.printStackTrace();
                bo = false;
            }
        }

        try {
            HandlerPrinter.realesePrinter(printer);
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
