package tp.printer.service;

import tp.ConnectionConfig;
import tp.printer.pool.HandlerPrinter;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ToPrint {

    private final ConnectionConfig cc = new ConnectionConfig();

    public ToPrint() throws IOException {
            start();
    }

    public void start() {
        boolean bo = true;

        try {
            ServerSocket listener = new ServerSocket(cc.getTcpPort());
            System.out.println("Printers readies");

            while ( bo ) {
                Socket socket = listener.accept();
                new PrinterManager( socket, HandlerPrinter.takePrinter() ).start();
            }

            listener.close();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            bo = false;
        }

    }

}
