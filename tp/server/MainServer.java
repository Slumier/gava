package tp.server;

import tp.server.gui.GUIServer;

import java.io.IOException;

public class MainServer {

    public static void main(String[] args) throws IOException {
        new GUIServer();
    }

}
