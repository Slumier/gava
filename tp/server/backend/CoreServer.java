package tp.server.backend;

import tp.ConnectionConfig;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class CoreServer {

    private final Socket socket;
    private String data;

    public CoreServer(Socket socket) throws IOException {
        this.socket = socket;
    }

    public String start() {
        try {
            OutputStream outputStream = socket.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

            dataOutputStream.writeUTF( getData() );
            System.out.println( "send :" +getData());
            dataOutputStream.flush();

            InputStream input = socket.getInputStream();
            DataInputStream dataInputStream = new DataInputStream(input);
            String msg = dataInputStream.readUTF();
            System.out.println("receive :" + msg);

            return msg;

        } catch (IOException e) {
            e.printStackTrace();
            return "error : CoreServer.java";
        }

    }

    public String sendData(String  data){
        this.data = data;
        return start();
    }

    public String getData(){
        return data;
    }

}
