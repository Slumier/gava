package tp.server.gui;

import tp.ConnectionConfig;
import tp.server.backend.CoreServer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.Socket;

public class GUIServer {

    private final ConnectionConfig cc = new ConnectionConfig();

    public GUIServer() throws IOException {
        frame();
    }

    public void frame() throws IOException {
        Socket socket = new Socket( cc.getIp(), cc.getTcpPort());
        CoreServer core = new CoreServer( socket);
        JFrame frame = new JFrame("Serveur - tp");
        JPanel content = new JPanel();
        String printer = core.sendData("getPool"); //To know how many printer are ready


        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        WindowListener listener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {

                try {
                    System.out.println("close");
                    socket.close();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }

            }
        };
        frame.addWindowListener(listener);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);

        JButton buttonAccept = new JButton("accepter");
        JButton buttonDeny = new JButton("refuser");

        String[] data = {"a","b","c","c","c","c","c","c","c","c","c","c","c"};

        DefaultListModel<String> l1 = new DefaultListModel<>();
        DefaultListModel<String> l2 = new DefaultListModel<>();
        DefaultListModel<String> l3 = new DefaultListModel<>();


        String[] printerList = printer.split("/");
        for ( int i = 0; i < printerList.length; i++) {
            l1.addElement(printerList[i]);
            l2.addElement("document ( id )");
        }
        JList<String> listPrinter = new JList<>(l1);
        JList<String> listDocument = new JList<>(l2);
        JList<String> listPrinted = new JList<>(l3);


        content.add(listPrinter);
        content.add(listDocument);

        buttonAccept.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (listDocument.getSelectedIndex() != -1) {

                    if(listPrinter.getSelectedIndex() != -1){
                        l3.addElement( core.sendData("accept/"+listPrinter.getSelectedValue()+"/"+listDocument.getSelectedValue()));
                        System.out.println( "accepte :" + listDocument.getSelectedValue() + " / " + listPrinter.getSelectedValue());
                    }
                }
            }
        });

        buttonDeny.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (listDocument.getSelectedIndex() != -1) {
                    core.sendData(listPrinter.getSelectedValue());
                    System.out.println( "refuse :" +  listPrinter.getSelectedValue());
                }
            }
        });

        content.add(buttonAccept);
        content.add(buttonDeny);
        content.add(listPrinted);

        frame.add(content);
        frame.setVisible(true);
    }

}
